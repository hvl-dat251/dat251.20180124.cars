package dat251.cars.problem;

public class Main {
    public static void main(String[] args) {
        
        CarCreator cc = new CarCreator();
        Car car1 = cc.createCar("familycar");
        Car car2 = cc.createCar("monstertruck");
        
        startRace(car1, car2);
        //...
    }
    
    public static void startRace(Car c1, Car c2) {
        c1.go();
        c2.go();
    }
}