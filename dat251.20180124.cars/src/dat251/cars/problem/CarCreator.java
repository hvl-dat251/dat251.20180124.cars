package dat251.cars.problem;

public class CarCreator {
    
    public Car createCar(String propname) {
    	
    	if (propname.equals("familycar")) {
    		return new FamilyCar();
    		
    	} else if (propname.equals("monstertruck")) {
    		return new MonsterTruck();
    		
    	} else {
    		return null;
    	}
    }
}
